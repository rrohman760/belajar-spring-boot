package belajar.springboot.akademik.dto;

public class AuthResponse {
    private String token;
    private UserData user;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public UserData getUser() {
        return user;
    }

    public void setUser(UserData user) {
        this.user = user;
    }
}
