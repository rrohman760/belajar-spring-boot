package belajar.springboot.akademik.dto;

import java.util.HashMap;
import java.util.Map;

public class ResponseData<T> {
    private boolean status;
    private Map<String, String> errors = new HashMap<>();
    private String message;
    private T data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Map<String, String> getErrors() {
        return errors;
    }

    public void setErrors(Map<String, String> errors) {
        this.errors = errors;
    }

    public void addErrors(String field, String errorMessage) {
        errors.put(field, errorMessage);
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
