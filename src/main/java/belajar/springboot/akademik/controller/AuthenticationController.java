package belajar.springboot.akademik.controller;

import belajar.springboot.akademik.dto.AuthResponse;
import belajar.springboot.akademik.dto.LoginRequest;
import belajar.springboot.akademik.dto.RegisterRequest;
import belajar.springboot.akademik.dto.ResponseData;
import belajar.springboot.akademik.service.AuthenticationService;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthenticationController {
    private final AuthenticationService authService;

    public AuthenticationController(AuthenticationService authService) {
        this.authService = authService;
    }

    @PostMapping("/register")
    public ResponseEntity<ResponseData<AuthResponse>> register(@Valid @RequestBody RegisterRequest request, Errors errors) {
        ResponseData<AuthResponse> responseData = new ResponseData<>();
        if (errors.hasErrors()){
            for (ObjectError error : errors.getAllErrors()){
                responseData.addErrors(getFieldName(error), error.getDefaultMessage());
            }
            responseData.setMessage("Failed to register");
            responseData.setStatus(false);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
        }

        try {
            responseData.setStatus(true);
            responseData.setMessage("Register successful");
            responseData.setData(authService.register(request));
            return ResponseEntity.status(HttpStatus.CREATED).body(responseData);
        }catch (Exception e){
            responseData.setStatus(false);
            responseData.setMessage("Register failed");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
        }
    }

    @PostMapping("/login")
    public ResponseEntity<ResponseData<AuthResponse>> login(@Valid @RequestBody LoginRequest request, Errors errors) {
        ResponseData<AuthResponse> responseData = new ResponseData<>();
        if (errors.hasErrors()){
            for (ObjectError error : errors.getAllErrors()){
                responseData.addErrors(getFieldName(error), error.getDefaultMessage());
            }
            responseData.setMessage("Failed to register");
            responseData.setStatus(false);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
        }
        try {
            responseData.setStatus(true);
            responseData.setMessage("Login successful");
            responseData.setData(authService.authenticate(request));
            return ResponseEntity.status(HttpStatus.OK).body(responseData);
        }catch (Exception e){
            responseData.setStatus(false);
            responseData.setMessage("Login failed");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
        }
    }

    private String getFieldName(ObjectError error) {
        if (error instanceof FieldError) {
            return ((FieldError) error).getField();
        }
        return error.getObjectName();
    }
}
