package belajar.springboot.akademik.controller;

import belajar.springboot.akademik.model.User;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {

    @GetMapping("/secured")
    public ResponseEntity<String> demo(@AuthenticationPrincipal User user) {
        return ResponseEntity.ok("Hello you login as " + user.getUsername());
    }
}
