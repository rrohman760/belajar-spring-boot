# Belajar menggunakan Spring Boot

Aplikasi ini adalah proyek belajar menggunakan Spring Boot berikut yang sudah implementasi:
1. JWT authentication
2. _Data Transfer Object_ (DTO)

## Instalasi

1. **Clone Repositori:**
   ```bash
   $ git clone https://gitlab.com/rrohman760/belajar-spring-boot.git
   $ cd belajar-spring-boot
2. **Konfigurasi Database**   
Sesuaikan file application.yml dengan informasi database anda.  
3. **Maven Build**
   ```bash
   $ mvn clean install
4. **Run Aplikasi**
   ```bash
   $ mvn spring-boot:run
